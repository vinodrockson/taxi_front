'use strict';

var app = angular.module('taxiyoApp');

app.service('PusherService', function ($rootScope) {
  var pusher = new Pusher('826ae640e19018fb2216');
  var channel = pusher.subscribe('bookings');
  return {
    onMessage: function (callback) {
      channel.bind('async_notification', function (data) {
        $rootScope.$apply(function () {
          callback(data);
        });
      });
    }
  };
});
