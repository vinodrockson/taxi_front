'use strict';

/**
 * @ngdoc overview
 * @name taxiyoApp
 * @description
 * # taxiyoApp
 *
 * Main module of the application.
 */
angular
  .module('taxiyoApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.bootstrap'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'OrderCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  })
  .run(function($rootScope){
     $rootScope.endPoint = 'https://damp-falls-7822.herokuapp.com'; //'http://localhost:3000';//
   });
