'use strict';
/**
 * @ngdoc function
 * @name taxiyoApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the taxiyoApp
 */
angular.module('taxiyoApp')
  .controller('OrderCtrl', function($scope, $http, $location, PusherService, $rootScope) {
  //$scope.message = '';
  $scope.alerts = [];
	$scope.placeOrder = function () {
		var order = { customer_name: $scope.name, customer_phone_num: $scope.phone, address: $scope.address, pick_up_time: $scope.date };
		 $http.post($rootScope.endPoint.concat('/orders'), order).success(function(data, status, headers, config) {
		 console.log(data);
     //$scope.message = data.message;
     $scope.alerts.push({msg: data.message});
        });
	};
  //$scope.asyncNotification = '';
  PusherService.onMessage(function(response) {
      $scope.alerts.push({msg: response.message});
      //$scope.asyncNotification = response.message;
  });
  $scope.closeAlert = function(index) {
    $scope.alerts.splice(index, 1);
  };
  });
