'use strict';

/**
 * @ngdoc function
 * @name taxiyoApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the taxiyoApp
 */
angular.module('taxiyoApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
